Intro
#Quoi
IC : integration continuer : 1..5

CD : Deploiment continue : Tester l'app dans env prod

1-Planifier  : repartition des taches --dans issues  
Jira, GitLab, Confluence, ALM Octane ou encore Pivotal Tracker.
2-compiler : assembler les compensants
Git, Subversion, GitHub, GitLab, Perforce ou bien Bitbucket.
Orchestrateur :   Jenkins, TeamCity, Azure DevOps, GitLab CI, Concours CI, Travis CI ou Bamboo.
Compiler avec : Maven, Ant, Gradle, MSBuild, NAnt, Gulp ou encore Grunt.
3-tester :  
JUnit, NUnit ou encore XUnit
4-mesurer la qualité : robuste ,fiable et reponds au besoins
SonarQube, Cast ou GitLab Code Quality.
5-stocker  : gestion des livrables (artefacts)
Nexus, Artifactory, GitLab repository, Quay, Docker Hub.
#Pourquoi
- moins de regressions
- verfications de code modifie
#Comment
- Gitlabs CI/CD
  test> build >quality>package
  Jenkins, TeamCity, Azure DevOps, GitLab CI, Concours CI, Travis CI ou Bamboo.





#Pour mettre en place la livraison continue, vous devez mettre en place 5 étapes :

1- La codification de l'infrastructure avec l'Infrastructure-as-Code.
Les outils principaux de l'Infrastructure-as-Code sont Docker, Chef, Puppet, Ansible et Terraform.



2-Le déploiement de votre application.
Pour pouvoir déployer les artefacts précédemment créés, vous pourrez utiliser Spinnaker, XLDeploy ou UrbanCode.
3-Le test de votre application en environnement de test.
Pour lancer des tests d'acceptance, vous pourrez utiliser Confluence, FitNesse ou Ranorex.
Pour faire des tests de performance, vous pourrez utiliser JMeter, Apache Bench ou Gatling, locust
4-La supervision de l'application.
Pour s'assurer du bon fonctionnement de l'application, vous pourrez utiliser Selenium, SoapUI ou Cypress.
Pour avoir un monitoring de vos applications, vous pourrez utiliser la suite Elastic, Prometheus ou Graylog.
Des outils comme Dynatrace, Sysdig ou New Relic permettent d'avoir ces métriques.

5-La mise en place de notifications d'alerte.

Pour avoir un feedback rapide de vos déploiements, vous pourrez tout simplement utiliser Slack, Trello ou Twitter.

#termes:
epic : regroupement US
user story : besoins de l'utilisateurs
task : taches
bug,
sprint,
Product Backlog,
board,
burndown chart,
Definition of Done,
Definition of Ready



https://labs.play-with-docker.com/p/c768avnnjsv000el5sp0
